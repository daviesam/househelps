package com.example.realhousehelps;

public class househelps_model {
    private int id;
    private String first_name;
    private String marital_status;
    private String dob;
    private double salary;
    private String type;
    private String imgURL;

    public househelps_model(int id, String first_name, String marital_status, String dob, double salary, String type, String imgURL) {
        this.id = id;
        this.first_name = first_name;
        this.marital_status = marital_status;
        this.dob = dob;
        this.salary = salary;
        this.type = type;
        this.imgURL = imgURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }
}