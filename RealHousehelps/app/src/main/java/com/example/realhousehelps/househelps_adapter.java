package com.example.realhousehelps;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Picasso;

import java.util.List;

public class househelps_adapter extends  RecyclerView.Adapter<househelps_adapter.househelpsViewHolder>  {
    private Context ctx;
    private List<househelps_model>HouseList;
    RequestOptions option;
    private OnItemClickListener mlistener;
    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

}
    public househelps_adapter(Context ctx, List<househelps_model> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.man).error(R.drawable.man);
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.view_househelps, null);
        return new househelpsViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull househelpsViewHolder holder, int i) {
         final househelps_model hh = HouseList.get(i);
String url="http://192.168.0.215/thumbnail/";
//        Glide.with(ctx)
//               .load(hh.getImgURL())
//                .thumbnail(0.5f)
//                .fitCenter()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//               .into(holder.imageView);
       // holder.imageView.setImageBitmap(bitmap);
        // Picasso.get().load(url+hh.getImgURL()).into(holder.imageView);
        Glide.with(ctx).load(url+hh.getImgURL()).apply(option).into(holder.imageView);
        holder.name.setText(hh.getFirst_name());
        holder.textdob.setText(hh.getDob());
        holder.texttype.setText(String.valueOf(hh.getType()));
        holder.textsalary.setText(String.valueOf(hh.getSalary()));
        holder.textStatus.setText(String.valueOf(hh.getMarital_status()));
        //holder.imageView.buildDrawingCache();
        //final Bitmap bmp = holder.imageView.getDrawingCache();
        //final Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx,househelp_details.class);
                intent.putExtra("id",hh.getId());
                ctx.startActivity(intent);
            }
        });
        //holder.imageView.setImageDrawable(ctx.getResources().getDrawable(hh.getImage()));
    }
    @Override
    public int getItemCount() {
        return HouseList.size();
    }

    class househelpsViewHolder extends  RecyclerView.ViewHolder{
        TextView name, textdob, texttype, textsalary,textStatus;
       ImageView imageView;

     public househelpsViewHolder(@NonNull View itemView) {
         super(itemView);
         name = itemView.findViewById(R.id.name);
         textdob = itemView.findViewById(R.id.dob);
         textsalary = itemView.findViewById(R.id.salary);
         textStatus=itemView.findViewById(R.id.status);
         texttype= itemView.findViewById(R.id.type);
         imageView = itemView.findViewById(R.id.imageView);
         imageView.buildDrawingCache();
         Bitmap bmp = imageView.getDrawingCache();
         itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(mlistener!=null){
                     int position= getAdapterPosition();
                     if(position!=RecyclerView.NO_POSITION){
                         mlistener.OnItemClick(position);
                     }
                 }
             }
         });
     }
 }


}
