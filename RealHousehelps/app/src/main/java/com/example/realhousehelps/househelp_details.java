package com.example.realhousehelps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class househelp_details extends AppCompatActivity {
final  String baseurl ="http://192.168.0.215/api/househelp/";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelp_details);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
loadHousehelps();

}
        public void loadHousehelps () {
            int number = getIntent().getExtras().getInt("id");


            //Toast.makeText(this, ""+number, Toast.LENGTH_LONG).show();
            progressDialog.setMessage("Loading HouseHelp Profile...");
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, baseurl+number,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                String url="http://192.168.0.215/thumbnail/";
                                //converting the string to json array object
                                JSONObject obj = new JSONObject(response);
                                JSONObject array = obj.getJSONObject("data");

                            TextView ftname = findViewById(R.id.fname);
                            TextView gender = findViewById(R.id.gender);
                            TextView status = findViewById(R.id.marriage);
                            TextView textdob = findViewById(R.id.dob);
                            TextView bio = findViewById(R.id.bio);
                            TextView lname = findViewById(R.id.lname);
                            ImageView image= findViewById(R.id.profile);
                            TextView placement = findViewById(R.id.placement);
                                    ftname.setText(array.getString("first_name"));
                                    lname.setText(array.getString("last_name"));
                                    gender.setText(array.getString("gender"));
                                    status.setText(array.getString("marital_status"));
                                    placement.setText(array.getString("type_of_placement"));
                                    textdob.setText(array.getString("dob"));
                                    bio.setText(array.getString("bio"));
                                    //String ur=array.getString("avatar");
//               Glide.with(getApplicationContext())
//               .load(url+ur)
//                .thumbnail(0.5f)
//                .fitCenter()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//               .into(image);

progressDialog.dismiss();
                            } catch (JSONException e) {
                                Toast.makeText(househelp_details.this, "ERROR" + e, Toast.LENGTH_LONG).show();

                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(" error",error.toString());
                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);


        }
    }

