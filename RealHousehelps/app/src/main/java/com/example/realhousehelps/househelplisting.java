package com.example.realhousehelps;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class househelplisting extends AppCompatActivity {
    String MAIN_URL = "http://192.168.0.215/api/househelps";
    List<househelps_model> houseList;
    ProgressDialog progressDialog;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelplisting);
        //getting the recyclerview from xml
        //recyclerView = findViewById(R.id.recyclerview);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        recyclerView = findViewById(R.id.rec);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        //initializing the houseList
        houseList = new ArrayList<>();

        //this method will fetch and parse json
        //to display it in recyclerview

        loadHousehelps();

    }
        private void loadHousehelps () {
            progressDialog.setMessage("Loading HouseHelps ...");
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, MAIN_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //converting the string to json array object
                                JSONObject obj = new JSONObject(response);
                                JSONArray array = obj.getJSONArray("data");
                               //Toast.makeText(househelplisting.this, "Data Received " + array, Toast.LENGTH_LONG).show();
                                //traversing through all the object
                                for (int i = 0; i < array.length(); i++) {
                                    //getting product object from json array
                                    JSONObject househelp = array.getJSONObject(i);
                                    //adding the househelps to  view

                                    houseList.add(new househelps_model(
                                            househelp.getInt("id"),
                                            househelp.getString("first_name"),
                                            househelp.getString("marital_status"),
                                            househelp.getString("dob"),
                                            househelp.getDouble("salary_expectation"),
                                            househelp.getString("type_of_placement"),
                                            househelp.getString("avatar")

                                    ));
                                }

                                //creating adapter object and setting it to recyclerview
                                househelps_adapter adapter = new househelps_adapter(househelplisting.this, houseList);
                                recyclerView.setAdapter(adapter);
                                progressDialog.dismiss();
                               // adapter.setOnItemClickListener(househelplisting.this);
                            } catch (JSONException e) {
                                Toast.makeText(househelplisting.this, "ERROR" + e, Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(" error",error.toString());
                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);
        }

}